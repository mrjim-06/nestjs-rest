# NestJS Rest API

## Dev tools
- [NodeJS](https://nodejs.org/en/download/)
- [Docker](https://www.docker.com/products/docker-desktop)
- [Visual Studio Code](https://code.visualstudio.com/download) 
- [Table plus](https://tableplus.com/)

## Build test database

You have to install [docker](https://www.docker.com/products/docker-desktop) first.

```bash
# start docker postgresql
$ npm run start:dev:db
```

## Database connection

> This project using PostgreSQL.  
> If you want to connect database from SQL client application please use information below to connect database.

- `HOST`: 127.0.0.1  
- `PORT`: 5432  
- `USER`: publicadmin  
- `PASSWORD`: publicadmin  

## Installation

```bash
# install npm package
$ npm install
```

## Running database migration

```bash
# generate migration file
$ npm run migration:generate -- {MIGRATION_NAME}

# running migration
$ npm run migration
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## License

Nest is [MIT licensed](LICENSE).
