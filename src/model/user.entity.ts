import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'user' })
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdate: Date;

  @Column({ type: 'varchar', length: 300 })
  firstname: string;

  @Column({ type: 'varchar', length: 300 })
  lastname: string;

  @Column({ type: 'varchar', length: 300 })
  mailaddress: string;

  @Column({ type: 'varchar', length: 300 })
  username: string;

  @Column({ type: 'varchar', length: 300 })
  password: string;
}
