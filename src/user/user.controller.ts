import { Body, Controller, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import { Result } from '../utils/result';
import { UserDto } from './user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private service: UserService) {}

  @Post()
  public async create(@Body() req: UserDto, @Res() res: Response) {
    const validated: Result = UserDto.validate(req);

    if (validated.ng())
      return res.status(validated.httpStatus()).send(validated.result());

    const result: Result = await this.service.register(req);
    return res.status(result.httpStatus()).send(result.result());
  }
}
