import { Injectable } from '@nestjs/common';
import { User } from '../model/user.entity';
import { Result } from '../utils/result';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserDto {
  readonly first_name: string;
  readonly last_name: string;
  readonly mail_address: string;
  readonly username: string;
  readonly password: string;

  public static validate(req: UserDto): Result {
    if (
      !req.first_name ||
      !req.last_name ||
      !req.mail_address ||
      !req.username ||
      !req.password
    )
      return Result.MISSING_REQUIRE_PARAMETER;

    return Result.SUCCESS;
  }

  public static async toEntity(dto: UserDto): Promise<User> {
    const user = new User();
    user.firstname = dto.first_name;
    user.lastname = dto.last_name;
    user.mailaddress = dto.mail_address;
    user.username = dto.username;
    user.password = await this.encryptedPassword(dto.password);
    user.createdate = new Date();
    return user;
  }

  private static async encryptedPassword(plaintext: string): Promise<string> {
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(plaintext, salt);
    return hashPassword;
  }
}
