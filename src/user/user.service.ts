import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../model/user.entity';
import { Result } from '../utils/result';
import { UserDto } from './user.dto';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private repository: Repository<User>) {}

  public async register(dto: UserDto): Promise<Result> {
    const exitsUser = await this.repository.find({
      where: [{ mailaddress: dto.mail_address }, { username: dto.username }],
    });

    if (exitsUser.length > 0) return Result.DUPLICATE_USER;

    const entity: User = await UserDto.toEntity(dto);
    await this.repository.save(entity);

    return Result.SUCCESS;
  }
}
