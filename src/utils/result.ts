import { HttpStatus } from '@nestjs/common';

export class Result {
  public static SUCCESS = new Result(0, HttpStatus.OK, 'Success.');
  public static CREATED = new Result(1, HttpStatus.CREATED, 'Created.');
  public static MISSING_REQUIRE_PARAMETER = new Result(
    101,
    HttpStatus.BAD_REQUEST,
    'Missing require parameter.',
  );
  public static DUPLICATE_USER = new Result(
    102,
    HttpStatus.CONFLICT,
    'Username or eamil address is already exits.',
  );

  constructor(
    private readonly code: number,
    private readonly status: HttpStatus,
    private readonly message: string,
  ) {}

  public errorCode(): number {
    return this.code;
  }

  public httpStatus(): number {
    return this.status;
  }

  public result() {
    return { code: this.code, message: this.message };
  }

  public ng(): boolean {
    return this != Result.CREATED && this != Result.SUCCESS;
  }
}
